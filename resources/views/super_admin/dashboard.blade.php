@extends('layouts.master')
@section('title', __('Dashboard'))
@section('content')
    <div class="row">
        <table>
            <tr>
                <th>Hotel</th>
                <th class="pl-3">Star Rating</th>
                <th class="pl-3">Revenue</th>
                <th class="pl-3"></th>
            </tr>
            @foreach($hotels as $hotel)
                <tr>
                    <td >{{$hotel->name}}</td>
                    <td class="pl-5">{{$hotel->star_rating}}</td>
                    <td class="pl-3">{{$hotel->revenue}}tk</td>
                </tr>
            @endforeach
        </table>

    </div>
@endsection
