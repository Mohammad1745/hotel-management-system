<?php

namespace App\Http\Controllers\Api\Hotel;

use App\Http\Requests\Api\CreateHotelDetailsRequest;
use App\Http\Requests\Api\CreateHotelFeatureRequest;
use App\Http\Requests\Api\CreateHotelRequest;
use App\Http\Requests\Api\DeleteHotelFeatureRequest;
use App\Http\Requests\Api\DeleteHotelRequest;
use App\Http\Requests\Api\UpdateHotelDetailsRequest;
use App\Http\Requests\Api\UpdateHotelFeatureRequest;
use App\Http\Requests\Api\UpdateHotelRequest;
use App\Http\Services\HotelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelController extends Controller
{
    private $service;

    /**
     * HotelController constructor.
     * @param HotelService $service
     */
    public function __construct(HotelService $service)
    {
        $this->service = $service;
    }

    /**
     * @param CreateHotelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateHotelRequest $request){
        $response = $this->service->createHotel($request);

        return response()->json($response);
    }

    /**
     * @param UpdateHotelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateHotelRequest $request){
        $response = $this->service->updateHotel($request);

        return response()->json($response);
    }

    /**
     * @param DeleteHotelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(DeleteHotelRequest $request){
        $response = $this->service->deleteHotel($request);

        return response()->json($response);
    }

    /**
     * @param CreateHotelDetailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDetails(CreateHotelDetailsRequest $request){
        $response = $this->service->createDetails($request);

        return response()->json($response);
    }

    /**
     * @param UpdateHotelDetailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDetails(UpdateHotelDetailsRequest $request){
        $response = $this->service->updateDetails($request);

        return response()->json($response);
    }

    /**
     * @param CreateHotelFeatureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFeature(CreateHotelFeatureRequest $request){
        $response = $this->service->createFeature($request);

        return response()->json($response);
    }

    /**
     * @param UpdateHotelFeatureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFeature(UpdateHotelFeatureRequest $request){
        $response = $this->service->updateFeature($request);

        return response()->json($response);
    }

    /**
     * @param DeleteHotelFeatureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFeature(DeleteHotelFeatureRequest $request){
        $response = $this->service->deleteFeature($request);

        return response()->json($response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function hotelIndex()
    {
        $response = $this->service->getAllHotels();

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function hotelFilter(Request $request)
    {
        $response = $this->service->hotelFilter($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function hotelDetails(Request $request)
    {
        $response = $this->service->getHotelDetails($request);

        return response()->json($response);
    }
}
