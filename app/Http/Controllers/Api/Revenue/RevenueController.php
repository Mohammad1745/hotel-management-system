<?php

namespace App\Http\Controllers\Api\Revenue;

use App\Http\Controllers\Controller;
use App\Http\Services\RevenueService;

class RevenueController extends Controller
{
    private $service;

    /**
     * RevenueController constructor.
     * @param RevenueService $service
     */
    public function __construct(RevenueService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRevenue()
    {
        $response = $this->service->getUserRevenue();

        return response()->json($response);
    }
}
