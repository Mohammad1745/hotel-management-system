<?php

namespace App\Http\Controllers\Api\History;

use App\Http\Controllers\Controller;
use App\Http\Services\HistoryService;

class HistoryController extends Controller
{
    private $service;

    /**
     * RevenueController constructor.
     * @param HistoryService $service
     */
    public function __construct(HistoryService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExpenditure()
    {
        $response = $this->service->getExpenditure();

        return response()->json($response);
    }
}
