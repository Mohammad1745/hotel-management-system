<?php

namespace App\Http\Controllers\Api\Room;

use App\Http\Requests\Api\CreateRoomRequest;
use App\Http\Requests\Api\DeleteRoomRequest;
use App\Http\Requests\Api\UpdateRoomRequest;
use App\Http\Requests\Api\UploadImageRequest;
use App\Http\Services\RoomService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomController extends Controller
{
    protected $service;

    /**
     * HotelInformationController constructor.
     * @param RoomService $service
     */
    public function __construct(RoomService $service)
    {
        $this->service = $service;
    }

    /**
     * @param CreateRoomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRoom(CreateRoomRequest $request){
        $response = $this->service->createRoom($request);

        return response()->json($response);
    }

    /**
     * @param UploadImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(UploadImageRequest $request){
        $response = $this->service->uploadImage($request);

        return response()->json($response);
    }

    /**
     * @param UpdateRoomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRoom(UpdateRoomRequest $request){
        $response = $this->service->updateRoom($request);

        return response()->json($response);
    }

    /**
     * @param DeleteRoomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRoom(DeleteRoomRequest $request){
        $response = $this->service->deleteRoom($request);

        return response()->json($response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function roomIndex()
    {
        $response = $this->service->getAllRooms();

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterRoom(Request $request)
    {
        $response = $this->service->filterRoom($request);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function roomDetails(Request $request)
    {
        $response = $this->service->getRoomDetails($request);

        return response()->json($response);
    }
}
