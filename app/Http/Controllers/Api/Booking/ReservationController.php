<?php

namespace App\Http\Controllers\Api\Booking;

use App\Http\Requests\Api\CheckInPaymentRequest;
use App\Http\Requests\Api\CheckInRequest;
use App\Http\Requests\Api\CheckOutRequest;
use App\Http\Services\ReservationService;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    private $service;

    /**
     * ReservationController constructor.
     * @param ReservationService $service
     */
    public  function __construct(ReservationService $service)
    {
        $this->service = $service;
    }

    /**
     * @param CheckInRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInRoom(CheckInRequest $request)
    {
        $response = $this->service->checkInRoom($request);

        return response()->json($response);
    }

    /**
     * @param CheckInPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInPayment(CheckInPaymentRequest $request)
    {
        $response = $this->service->checkInPayment($request);

        return response()->json($response);
    }

    /**
     * @param CheckOutRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkOutRoom(CheckOutRequest $request)
    {
        $response = $this->service->checkOutRoom($request);

        return response()->json($response);
    }
}
