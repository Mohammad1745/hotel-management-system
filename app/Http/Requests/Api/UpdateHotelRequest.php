<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UpdateHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'name' => 'required|string|min:3|max:40',
            'star_rating' => 'required|numeric|gte:2|lte:5',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => __('Id can not be empty.'),
            'id.numeric' => __('Id must be a number.'),
            'name.required' => __('Name can not be empty.'),
            'name.min' => __('Name must be at least 3 character.'),
            'name.max' => __('Name cannot be more than 40 character.'),
            'star_rating.required' => __('Star Rating can not be empty.'),
            'star_rating.gte' => __('Star Rating can be 2, 3 ,4 or 5.'),
            'star_rating.lte' => __('Star Rating can be 2, 3 ,4 or 5..'),
        ];
    }

    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = '';
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors .= $error . "\n";
                }
            }
            $json = [
                'success' => false,
                'message' => $errors,
                'data' => null
            ];
            $response = new JsonResponse($json, 422);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }
    }
}
