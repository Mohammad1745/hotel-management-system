<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UpdateHotelDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'hotel_id' => 'required|numeric',
            'country' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'location' => 'required|string',
            'zip_code' => 'required|numeric',
        ];
    }

//    /**
//     * @return array
//     */
//    public function messages()
//    {
//        return [
//            'id.required' => __('Id can not be empty.'),
//            'id.numeric' => __('Id must be a number.'),
//            'hotel_id.required' => __('Hotel id can not be empty.'),
//            'hotel_id.numeric' => __('Hotel id must be a number.'),
//            'country.required' => __('Country can not be empty.'),
//            'country.string' => __('Country must be an string.'),
//            'city.string' => __('City can not be empty.'),
//            'city.required' => __('City must be an string.'),
//            'state.string' => __('State can not be empty.'),
//            'state.required' => __('State must be an string.'),
//            'location.string' => __('Location can not be empty.'),
//            'location.required' => __('Location must be an string.'),
//            'zip_code.required' => __('Zip Code can not be empty.'),
//            'zip_code.numeric' => __('Zip Code must be a number.'),
//        ];
//    }

    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = '';
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors .= $error . "\n";
                }
            }
            $json = [
                'success' => false,
                'message' => $errors,
                'data' => null
            ];
            $response = new JsonResponse($json, 422);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }
    }
}
