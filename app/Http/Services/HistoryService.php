<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 8/9/19
 * Time: 12:21 PM
 */

namespace App\Http\Services;


use App\Http\Repository\RevenueRepository;
use Illuminate\Support\Facades\Auth;

class HistoryService
{
    private $revenueRepository;

    /**
     * RoomService constructor.
     */
    public function __construct()
    {
        $this->revenueRepository = new RevenueRepository();
    }

    /**
     * @return array
     */
    public function getExpenditure()
    {
        $user = Auth::user();
        if($user->role == USER_ROLE){
            try{
                $data = $this->revenueRepository->getExpenditure($user);
                return [
                    'success' => true,
                    'message' => '',
                    'data' => $data
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
}
