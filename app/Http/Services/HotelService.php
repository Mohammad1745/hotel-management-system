<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 8/9/19
 * Time: 12:21 PM
 */

namespace App\Http\Services;


use App\Http\Repository\HotelDetailRepository;
use App\Http\Repository\HotelFeatureRepository;
use App\Http\Repository\HotelRepository;
use App\Http\Repository\RoomRepository;
use Illuminate\Support\Facades\Auth;

class HotelService
{
    private $hotelRepository;
    private $hotelDetailRepository;
    private $hotelFeatureRepository;
    private $roomRepository;

    /**
     * HotelService constructor.
     */
    public function __construct()
    {
        $this->hotelRepository= new HotelRepository();
        $this->hotelDetailRepository= new HotelDetailRepository();
        $this->hotelFeatureRepository= new HotelFeatureRepository();
        $this->roomRepository= new RoomRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function createHotel($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $data = [
                'user_id' => Auth::user()->id,
                'name' => $request->name,
                'star_rating' => $request->star_rating == null ? TWO : $request->star_rating,
            ];
            try{
                $data = $this->hotelRepository->create($data);

                return [
                    'success' => true,
                    'message' => __('Hotel has been created successfully.'),
                    'data' => $data
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateHotel($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'user_id' => $user->id
            ];
            $data = [
                'name' => $request->name,
                'star_rating' => $request->star_rating
            ];
            try{
                $success = $this->hotelRepository->update($where, $data);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Hotel has been updated successfully.'),
                        'data' => $data
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Hotel cannot be edited'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteHotel($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'user_id' => $user->id
            ];
            try{
                $success = $this->hotelRepository->deleteWhere($where);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Hotel has been deleted successfully.'),
                        'data' => null
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Hotel cannot be deleted'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function createDetails($request)
    {
        $user = Auth::user();
        $where = [
          'id' => $request->hotel_id,
          'user_id' => $user->id
        ];
        $hotel = $this->hotelRepository->whereFirst($where);
        if($hotel && $user->role == ADMIN_ROLE){
            $data = [
                'hotel_id' => $request->hotel_id,
                'country' => $request->country,
                'city' => $request->city,
                'state' => $request->state,
                'location' => $request->location,
                'zip_code' => $request->zip_code,
            ];
            try{
                $data = $this->hotelDetailRepository->create($data);
                return [
                    'success' => true,
                    'message' => __('Hotel Details has been added successfully.'),
                    'data' => $data
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateDetails($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id
            ];
            $data = [
                'country' => $request->country,
                'city' => $request->city,
                'state' => $request->state,
                'location' => $request->location,
                'zip_code' => $request->zip_code,
            ];
            try{
                $success = $this->hotelDetailRepository->update($where, $data);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Hotel Details has been updated successfully.'),
                        'data' => $data
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Hotel Details cannot be edited'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function createFeature($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'hotel_id' => $request->hotel_id,
                'feature' => $request->feature,
            ];
            $data = [
                'feature' => $request->feature,
            ];
            try{
                $data = $this->hotelFeatureRepository->updateOrCreate($where, $data);

                return [
                    'success' => true,
                    'message' => __('Hotel Feature has been added successfully.'),
                    'data' => $data
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateFeature($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id,
            ];
            $data = [
                'feature' => $request->feature,
            ];
            try{
                $success = $this->hotelFeatureRepository->update($where, $data);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Hotel Feature has been updated successfully.'),
                        'data' => $data
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Hotel Feature cannot be edited'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteFeature($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id,
            ];
            try{
                $success = $this->hotelFeatureRepository->deleteWhere($where);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Hotel Feature has been deleted successfully.'),
                        'data' => null
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Hotel Feature cannot be deleted'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @return array
     */
    public function getAllHotels()
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            $data = $this->hotelRepository->getAll();

            return [
                'success' => true,
                'message' => __(''),
                'data' => $data
            ];
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function hotelFilter($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            try{
                $data = [];
                if($request->name){
                    $where = [
                        ['name', 'like', '%' .  $request->name . '%']
                    ];
                    $data = $this->hotelRepository->getWhere($where);
                }
                if(empty($data) && $request->except('name')){
                    $where = [];
                    if($request->star_rating)   $where['hotels.star_rating'] = $request->star_rating;
                    if($request->country)       $where['hotel_details.country'] = $request->country;
                    if($request->city)          $where['hotel_details.city'] = $request->city;
                    if($request->state)         $where['hotel_details.state'] = $request->state;
                    if($request->location)      $where['hotel_details.location'] = $request->location;
                    if($request->feature)       $where['hotel_features.feature'] = $request->feature;
                    $data = $this->hotelRepository->filter($where);
                }
                if(empty($data)){
                    return [
                        'success' => false,
                        'message' => __('Hotel not found.'),
                        'data' => null
                    ];
                }
                else{
                    return [
                        'success' => true,
                        'message' => __(''),
                        'data' => $data
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function getHotelDetails($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            try{
                $where = ['hotel_id' => $request->hotel_id];
                $details = $this->hotelDetailRepository->whereFirst($where);
                $features = $this->hotelFeatureRepository->getWhere($where);
                $rooms = $this->roomRepository->getWhere($where);

                return [
                    'success' => true,
                    'message' => __('Got you'),
                    'data' => [
                        'details' => $details,
                        'features' => $features,
                        'rooms' => $rooms
                    ]
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
}
