<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 8/9/19
 * Time: 12:21 PM
 */

namespace App\Http\Services;


use App\Http\Repository\HotelRepository;
use App\Http\Repository\RevenueRepository;
use Illuminate\Support\Facades\Auth;

class RevenueService
{
    private $hotelRepository;
    private $revenueRepository;

    /**
     * RoomService constructor.
     */
    public function __construct()
    {
        $this->hotelRepository = new HotelRepository();
        $this->revenueRepository = new RevenueRepository();
    }

    /**
     * @return array
     */
    public function getUserRevenue()
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            try{
                $data = $this->revenueRepository->getUserRevenue($user);
                return [
                    'success' => true,
                    'message' => '',
                    'data' => $data
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @return array
     */
    public function getAllRevenue()
    {
        $user = Auth::user();
        if($user->role == SUPER_ADMIN_ROLE){
            try{
                $hotels = $this->hotelRepository->getAll();
                foreach ($hotels as $hotel){
                    $hotel['revenue'] = $this->revenueRepository->sumWhere(['hotel_id' => $hotel->id], 'amount');
                }

                return [
                    'success' => true,
                    'message' => '',
                    'data' => $hotels
                ];
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
}
