<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 8/9/19
 * Time: 12:21 PM
 */

namespace App\Http\Services;


use App\Http\Repository\HotelRepository;
use App\Http\Repository\RevenueRepository;
use App\Http\Repository\RoomImageRepository;
use App\Http\Repository\RoomRepository;
use App\Http\Repository\RoomReservationRepository;
use App\Jobs\SendPaymentConfirmedEmailJob;
use App\Jobs\SendRoomReservationConfirmedEmailJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReservationService
{
    private $hotelRepository;
    private $roomRepository;
    private $roomImageRepository;
    private $roomReservationRepository;
    private $revenueRepository;

    /**
     * HotelService constructor.
     */
    public function __construct()
    {
        $this->hotelRepository = new HotelRepository();
        $this->roomRepository = new RoomRepository();
        $this->roomImageRepository = new RoomImageRepository();
        $this->roomReservationRepository = new RoomReservationRepository();
        $this->revenueRepository = new RevenueRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function checkInRoom($request)
    {
        $user = Auth::user();
        if($user->role==USER_ROLE){
            $where = ['id' => $request->hotel_id,];
            $hotel = $this->hotelRepository->whereFirst($where);
            $where = [
                ['id', $request->room_id],
                ['hotel_id', $request->hotel_id],
                ['reservation_status', false],
                ['available_at', '<', $request->check_in]
            ];
            $room = $this->roomRepository->whereFirst($where);
            if($hotel && $room){
                try{
                    DB::beginTransaction();
                    $check_in = new Carbon($request->check_in);
                    $check_out = new Carbon($request->check_in);
                    $check_out->addDays($request->duration_of_stay);
                    $data = [
                        'reservation_status' => true,
                        'available_at' => $check_out
                    ];
                    $this->roomRepository->update($where, $data);
                    $data = [
                        'user_id' => $user->id,
                        'room_id' => $room->id,
                        'check_in' => $check_in,
                        'check_out' => $check_out,
                        'payment_status' => BOOKED,
                        'paid_amount' => 0,
                    ];
                    $data = $this->roomReservationRepository->create($data);
                    $defaultEmail = 'boilerplate@email.com';
                    $defaultName = 'Boiler Plate';
                    $logo =  asset('assets/images/laravelLogo.png');
                    dispatch(new SendRoomReservationConfirmedEmailJob($hotel, $room, $defaultName, $logo, $user, $defaultEmail))->onQueue('email-send');
                    DB::commit();

                    return [
                        'success' => true,
                        'message' => __('Congratulation! Reservation is successful.'),
                        'data' => $data
                    ];
                }catch (\Exception $exception){
                    DB::rollBack();
                    return [
                        'success' => false,
                        'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                        'data' => null
                    ];
                }
            }
            else{
                return [
                    'success' => false,
                    'message' => __('Sorry! Room is not available right now. Try another.'),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function checkInPayment($request)
    {
        $user = Auth::user();
        if($user->role==USER_ROLE){
            $where = ['id' => $request->hotel_id,];
            $hotel = $this->hotelRepository->whereFirst($where);
            $where = [
                'id'=> $request->room_id,
                'hotel_id'=> $request->hotel_id
            ];
            $room = $this->roomRepository->whereFirst($where);
            $where = [
                'room_id' => $request->room_id,
                'user_id' => $user->id
            ];
            $orderBy = ['id', 'desc'];
            $reservation = $this->roomReservationRepository->whereFirst($where, $orderBy);
            if($hotel && $room && $reservation){
                if($reservation->payment_status != PAID){
                    if($request->paid_amount == $room->rent){
                        try{
                            DB::beginTransaction();
                            $where = ['id' => $reservation->id];
                            $data = [
                                'user_id' => $user->id,
                                'room_id' => $room->id,
                                'payment_status' => PAID,
                                'paid_amount' => $request->paid_amount,
                            ];
                            $this->roomReservationRepository->update($where, $data);
                            $data = [
                                'hotel_id' => $room->hotel_id,
                                'room_id' => $room->id,
                                'user_id' => $user->id,
                                'amount' => $request->paid_amount
                            ];
                            $this->revenueRepository->create($data);
                            $defaultEmail = 'boilerplate@email.com';
                            $defaultName = 'Boiler Plate';
                            $logo =  asset('assets/images/laravelLogo.png');
                            dispatch(new SendPaymentConfirmedEmailJob($hotel, $room, $defaultName, $logo, $user, $defaultEmail))->onQueue('email-send');
                            DB::commit();

                            return [
                                'success' => true,
                                'message' => __('Payment confirmed.'),
                                'data' => $data
                            ];
                        }catch (\Exception $exception){
                            DB::rollBack();

                            return [
                                'success' => false,
                                'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                                'data' => null
                            ];
                        }
                    }
                    else{
                        return [
                            'success' => false,
                            'message' => __('You paid ') . $request->paid_amount . __('. Please, Pay ') . $room->rent .__(' for the room.') ,
                            'data' => null
                        ];
                    }
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('You have already paid for the room!') ,
                        'data' => null
                    ];
                }
            }
            else{
                return [
                    'success' => false,
                    'message' => __('Something went wrong. Please try again!'),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function checkOutRoom($request)
    {
        $user = Auth::user();
        if($user->role==USER_ROLE){
            $where = [
                'id'=> $request->room_id,
                'hotel_id'=> $request->hotel_id
            ];
            $room = $this->roomRepository->whereFirst($where);
            $where = [
                ['room_id', $request->room_id],
                ['user_id', $user->id]
            ];
            $orderBy = ['id', 'desc'];
            $reservation = $this->roomReservationRepository->whereFirst($where, $orderBy);
            if($room && $reservation){
                if($room->reservation_status && $reservation->check_out>Carbon::now()){
                    if($reservation->paid_amount == $room->rent){
                        try{
                            $where = ['id' => $room->id];
                            $data = [
                                'reservation_status' => false,
                                'available_at' => Carbon::now()
                            ];
                            $this->roomRepository->update($where, $data);
                            $where = ['id' => $reservation->id];
                            $data = ['check_out' => Carbon::now()];
                            $this->roomReservationRepository->update($where, $data);

                            return [
                                'success' => true,
                                'message' => __('Check Out confirmed.'),
                                'data' => null
                            ];
                        }catch (\Exception $exception){
                            return [
                                'success' => false,
                                'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                                'data' => null
                            ];
                        }
                    }
                    else{
                        return [
                            'success' => false,
                            'message' => __('You paid ') . $request->paid_amount . __('. Please, Pay ') . $room->rent .__(' for the room.') ,
                            'data' => null
                        ];
                    }
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('The room is not reserved by you') ,
                        'data' => null
                    ];
                }
            }
            else{
                return [
                    'success' => false,
                    'message' => __('Something went wrong. Please try again!'),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
}
