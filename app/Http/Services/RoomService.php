<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 8/9/19
 * Time: 12:21 PM
 */

namespace App\Http\Services;


use App\Http\Repository\HotelRepository;
use App\Http\Repository\RoomImageRepository;
use App\Http\Repository\RoomRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoomService
{
    private $hotelRepository;
    private $roomRepository;
    private $roomImageRepository;

    /**
     * RoomService constructor.
     */
    public function __construct()
    {
        $this->hotelRepository= new HotelRepository();
        $this->roomRepository= new RoomRepository();
        $this->roomImageRepository= new RoomImageRepository();
    }

    /**
     * @param $request
     * @return array
     */
    public function createRoom($request)
    {
        $user = Auth::user();
        $where = [
            'id' => $request->hotel_id,
            'user_id' => $user->id
        ];
        $hotel = $this->hotelRepository->whereFirst($where);
        if($hotel && $user->role == ADMIN_ROLE){
            $type = $request->room_type=='single' ? SINGLE : ($request->room_type=='double' ? DOUBLE : DELUX);
            $smoking = $request->smoking_zone=='yes';
            $where = [
                'hotel_id' => $request->hotel_id,
                'floor_no' => $request->floor_no,
                'room_no' => $request->room_no,
            ];
            $data = [
                'room_type' => $type,
                'rent' => $request->rent,
                'smoking_zone' => $smoking,
                'reservation_status' => false,
                'available_at' => Carbon::now()
            ];
            try{
                $data = $this->roomRepository->updateOrCreate($where, $data);

                return [
                    'success' => true,
                    'message' => __('Room has been created successfully.'),
                    'data' => $data
                ];
            }catch (\Exception $exception){
                DB::rollBack();

                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function uploadImage($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id,
            ];
            $room = $this->roomRepository->whereFirst($where);
            if ($room && $request->hasFile('picture')) {
                try{
                    $picture = uploadFile($request->picture, roomPath());
                    $roomImage = [
                        'room_id' => $room->id,
                        'picture' => $picture
                    ];
                    $data = $this->roomImageRepository->create($roomImage);
                    return [
                        'success' => true,
                        'message' => __('Image has been uploaded successfully.'),
                        'data' => $roomImage
                    ];
                }catch (\Exception $exception){
                    return [
                        'success' => false,
                        'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                        'data' => null
                    ];
                }
            }
            else{
                return [
                    'success' => false,
                    'message' => __('Room not found'),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
    /**
     * @param $request
     * @return array
     */
    public function updateRoom($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $type = $request->room_type=='single' ? SINGLE : ($request->room_type=='double' ? DOUBLE : DELUX);
            $smoking = $request->smoking_zone=='yes';
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id,
            ];
            $data = [
                'floor_no' => $request->floor_no,
                'room_no' => $request->room_no,
                'room_type' => $type,
                'rent' => $request->rent,
                'smoking_zone' => $smoking,
            ];
            try{
                $success = $this->roomRepository->update($where, $data);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Room has been updated successfully.'),
                        'data' => $data
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Room cannot be edited'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function deleteRoom($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id
            ];
            try{
                $success = $this->roomRepository->deleteWhere($where);
                if($success){
                    return [
                        'success' => true,
                        'message' => __('Room has been deleted successfully.'),
                        'data' => null
                    ];
                }
                else{
                    return [
                        'success' => false,
                        'message' => __('Room cannot be deleted'),
                        'data' => null
                    ];
                }
            }catch (\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @return array
     */
    public function getAllRooms()
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            $data = $this->roomRepository->getAll();

            return [
                'success' => true,
                'message' => __(''),
                'data' => $data
            ];
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array
     * @throws \Exception
     */
    public function filterRoom($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            try{
                $query = $this->generateFilterQuery($request);
                $data = $this->roomRepository->filter($query['where'], $query['price'], $query['date']);
                if(empty($data)){
                    return [
                        'success' => false,
                        'message' => __('Room not found.'),
                        'data' => null
                    ];
                }
                else{
                    return [
                        'success' => true,
                        'message' => __(''),
                        'data' => $data
                    ];
                }
            }catch(\Exception $exception){
                return [
                    'success' => false,
                    'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }

    /**
     * @param $request
     * @return array|array[]
     * @throws \Exception
     */
    private function generateFilterQuery($request)
    {
        $where = [];
        if($request->star_rating)   $where['hotels.star_rating'] = $request->star_rating;
        if($request->country)       $where['hotel_details.country'] = $request->country;
        if($request->city)          $where['hotel_details.city'] = $request->city;
        if($request->state)         $where['hotel_details.state'] = $request->state;
        if($request->location)      $where['hotel_details.location'] = $request->location;
        if($request->feature)       $where['hotel_features.feature'] = $request->feature;
        if($request->smoking_zone)  $where['rooms.smoking_zone'] = $request->smoking_zone=='yes';
        if($request->room_type){
            $type = $request->room_type=='single' ? SINGLE : ($request->room_type=='double' ? DOUBLE : DELUX);
            $where['rooms.room_type'] = $type;
        }
        if($request->price_range){
            $price = explode(',',$request->price_range);
        }
        else{
            $price = [
                $this->roomRepository->minWhere([], 'rent'),
                $this->roomRepository->maxWhere([], 'rent')
            ];
        }
        if($request->date_range){
            $dateRange = explode(',',$request->date_range);
            $date = [ new Carbon($dateRange[0]), new Carbon($dateRange[1])];
        }
        else{
            $date = [
                $this->roomRepository->minWhere([], 'available_at'),
                $this->roomRepository->maxWhere([], 'available_at')
            ];
        }

        return [
            'where' => $where,
            'price' => $price,
            'date' => $date
        ];
    }

    /**
     * @param $request
     * @return array
     */
    public function getRoomDetails($request)
    {
        $user = Auth::user();
        if($user->role == ADMIN_ROLE || $user->role == USER_ROLE){
            $where = [
                'id' => $request->id,
                'hotel_id' => $request->hotel_id
            ];
            $room = $this->roomRepository->whereFirst($where);
            if($room){
                try{
                    $where = ['room_id' => $room->id];
                    $images = $this->roomImageRepository->getWhere($where);
                    $i=0;
                    foreach ($images as $value){
                        $images[$i++] = asset(roomPath()) . '/' . $value['picture'];
                    }

                    return [
                        'success' => true,
                        'message' => '',
                        'data' => [
                            'details' => $room,
                            'images' => $images
                        ]
                    ];
                }catch(\Exception $exception){
                    return [
                        'success' => false,
                        'message' =>  __('Something went wrong. Please try again! ') . $exception->getMessage(),
                        'data' => null
                    ];
                }
            }
            else{
                return [
                    'success' => false,
                    'message' => __('Room does not exist.'),
                    'data' => null
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => __('Unauthorized request'),
                'data' => null
            ];
        }
    }
}
