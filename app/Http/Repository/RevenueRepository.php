<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\Revenue;

class RevenueRepository extends CommonRepository
{
    public $model;

    /**
     * RevenueRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Revenue();
        parent::__construct($this->model);
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getUserRevenue($user)
    {
        return Revenue::select([
            'hotels.name as hotel',
            'rooms.floor_no as floor',
            'rooms.room_no as room',
            'revenues.amount as revenue',
            'revenues.created_at as date',
            'users.first_name as consumer_first_name',
            'users.last_name as consumer_last_name',
        ])
            ->leftjoin('hotels', ['revenues.hotel_id' => 'hotels.id'])
            ->leftjoin('rooms', ['revenues.room_id' => 'rooms.id'])
            ->leftjoin('users', ['revenues.user_id' => 'users.id'])
            ->where('hotels.user_id', $user->id)
            ->orderBy('revenues.id', 'desc')
            ->get();
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getExpenditure($user)
    {
        return Revenue::select([
            'hotels.name as hotel',
            'rooms.floor_no as floor',
            'rooms.room_no as room',
            'revenues.amount as revenue',
            'revenues.created_at as date',
        ])
        ->leftjoin('hotels', ['revenues.hotel_id' => 'hotels.id'])
        ->leftjoin('rooms', ['revenues.room_id' => 'rooms.id'])
        ->leftjoin('users', ['revenues.user_id' => 'users.id'])
        ->where('revenues.user_id', $user->id)
        ->orderBy('revenues.id', 'desc')
        ->get();
    }
}
