<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\HotelFeature;

class HotelFeatureRepository extends CommonRepository
{
    public $model;

    /**
     * HotelFeatureRepository constructor.
     */
    public function __construct()
    {
        $this->model = new HotelFeature();
        parent::__construct($this->model);
    }
}
