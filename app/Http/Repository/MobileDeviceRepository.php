<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;



use App\Models\MobileDevice;

class MobileDeviceRepository extends CommonRepository
{
    public $model;

    /**
     * MobileDeviceRepository constructor.
     */
    public function __construct()
    {
        $this->model = new MobileDevice();
        parent::__construct($this->model);
    }
}
