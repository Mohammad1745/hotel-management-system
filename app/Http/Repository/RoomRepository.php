<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\Hotel;
use App\Models\Room;

class RoomRepository extends CommonRepository
{
    public $model;

    /**
     * RoomRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Room();
        parent::__construct($this->model);
    }

    /**
     * @param $where
     * @param $price
     * @param $date
     * @return mixed
     */
    public function filter($where, $price, $date)
    {
        return Room::select([
            'hotels.name as hotel',
            'hotels.star_rating as star_rating',
            'hotel_details.location as location',
            'rooms.room_no as room_no',
            'rooms.room_type as room_type',
            'rooms.rent as rent',
            'rooms.reservation_status as reservation_status',
            'rooms.available_at as available_at',
        ])
        ->leftjoin('hotels', ['rooms.hotel_id' => 'hotels.id'])
        ->leftjoin('hotel_details', ['hotel_details.hotel_id' => 'hotels.id'])
        ->leftjoin('hotel_features', ['hotel_features.hotel_id' => 'hotels.id'])
        ->where($where)
        ->whereBetween('rooms.rent', $price)
        ->whereBetween('rooms.available_at', $date)
        ->distinct('rooms.id')
        ->get();
    }
}
