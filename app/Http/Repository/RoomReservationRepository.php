<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\RoomReservation;

class RoomReservationRepository extends CommonRepository
{
    public $model;

    /**
     * RoomReservationRepository constructor.
     */
    public function __construct()
    {
        $this->model = new RoomReservation();
        parent::__construct($this->model);
    }
}
