<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\RoomImage;

class RoomImageRepository extends CommonRepository
{
    public $model;

    /**
     * RoomImageRepository constructor.
     */
    public function __construct()
    {
        $this->model = new RoomImage();
        parent::__construct($this->model);
    }
}
