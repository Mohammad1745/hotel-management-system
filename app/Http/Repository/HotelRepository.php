<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;


use App\Models\Hotel;

class HotelRepository extends CommonRepository
{
    public $model;

    /**
     * HotelRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Hotel();
        parent::__construct($this->model);
    }

    /**
     * @param $where
     * @return mixed
     */
    public function filter($where)
    {
        return Hotel::select([
            'hotels.name as name',
            'hotels.star_rating as star_rating',
            'hotel_details.location as location',
        ])
        ->leftjoin('hotel_details', ['hotel_details.hotel_id' => 'hotels.id'])
        ->leftjoin('hotel_features', ['hotel_features.hotel_id' => 'hotels.id'])
        ->where($where)
        ->distinct('hotels.id')
        ->get();
    }
}
