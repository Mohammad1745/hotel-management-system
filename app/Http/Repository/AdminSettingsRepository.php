<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;



use App\Models\AdminSetting;

class AdminSettingsRepository extends CommonRepository
{
    public $model;

    /**
     * AdminSettingsRepository constructor.
     */
    public function __construct()
    {
        $this->model = new AdminSetting();
        parent::__construct($this->model);
    }
}
