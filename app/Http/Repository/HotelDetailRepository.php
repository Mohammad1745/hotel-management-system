<?php
/**
 * Created by PhpStorm.
 * User: debu
 * Date: 7/5/19
 * Time: 7:31 PM
 */

namespace App\Http\Repository;

use App\Models\HotelDetail;

class HotelDetailRepository extends CommonRepository
{
    public $model;

    /**
     * HotelDetailRepository constructor.
     */
    public function __construct()
    {
        $this->model = new HotelDetail();
        parent::__construct($this->model);
    }
}
