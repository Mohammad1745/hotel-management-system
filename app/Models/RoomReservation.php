<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RoomReservation extends Model
{
    protected $fillable = ['user_id', 'room_id', 'check_in', 'check_out', 'payment_status', 'paid_amount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
