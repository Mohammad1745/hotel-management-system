<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = ['user_id', 'name', 'star_rating'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hotelDetails()
    {
        return $this->hasOne(HotelDetail::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotelFeatures()
    {
        return $this->hasMany(HotelFeature::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function revenues()
    {
        return $this->hasMany(Revenue::class);
    }
}
