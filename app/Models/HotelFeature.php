<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelFeature extends Model
{
    protected $fillable = ['hotel_id', 'feature'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
