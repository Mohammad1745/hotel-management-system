<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendPaymentConfirmedEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $hotel;
    protected $room;
    protected $defaultName;
    protected $logo;
    protected $user;
    protected $defaultEmail;

    /**
     * Create a new job instance.
     *
     * @param $defaultName
     * @param $logo
     * @param $user
     * @param $defaultEmail
     * @param $hotel
     * @param $room
     */
    public function __construct($hotel, $room, $defaultName, $logo, $user, $defaultEmail)
    {
        $this->hotel = $hotel;
        $this->room = $room;
        $this->defaultName = $defaultName;
        $this->logo = $logo;
        $this->user = $user;
        $this->defaultEmail = $defaultEmail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $user = $this->user;
            $defaultEmail = $this->defaultEmail;
            $defaultName = $this->defaultName;
            Mail::send('email.payment_confirmation', ['hotel' => $this->hotel, 'room' => $this->room, 'company' => $this->defaultName, 'logo' => $this->logo], function ($message) use ($user, $defaultEmail, $defaultName) {
                $message->to($user->email)->subject(__('Payment Confirmed'))->from(
                    $defaultEmail, $defaultName
                );
            });
        }catch (\Exception $exception){
            Log::info($exception->getMessage());
        }
    }
}
