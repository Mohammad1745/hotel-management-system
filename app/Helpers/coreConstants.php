<?php

const USER_PENDING_STATUS = 0;
const USER_ACTIVE_STATUS = 1;


const SUPER_ADMIN_ROLE = 1;
const ADMIN_ROLE = 2;
const USER_ROLE = 3;

const PENDING_STATUS = 0;
const ACTIVE_STATUS = 1;
const DELETE_STATUS = 5;

const EXPIRE_TIME_OF_FORGET_PASSWORD_CODE = 10;

const TWO = 2;
const THREE = 3;
const FOUR = 4;
const FIVE = 5;

const SINGLE = 1;
const DOUBLE = 2;
const DELUX = 3;

const BOOKED = 1;
const DUE = 2;
const PAID = 3;
