<?php


use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['auth:api', 'app.user', 'verified.user'], 'namespace' => 'Api\Booking'], function () {
    Route::post('check-in-room', 'ReservationController@checkInRoom');
    Route::post('check-in-payment', 'ReservationController@checkInPayment');
    Route::post('check-out-room', 'ReservationController@checkOutRoom');
});
