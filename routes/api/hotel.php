<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'verified.user'], 'namespace' => 'Api\Hotel'], function () {
    Route::get('get-all-hotels', 'HotelController@hotelIndex');
    Route::post('filter-hotels', 'HotelController@hotelFilter');
    Route::post('hotel-details', 'HotelController@hotelDetails');

    Route::group(['middleware' => 'admin'], function () {

        Route::post('create-hotel', 'HotelController@create');
        Route::post('update-hotel', 'HotelController@update');
        Route::post('delete-hotel', 'HotelController@delete');
        Route::post('create-hotel-details', 'HotelController@createDetails');
        Route::post('update-hotel-details', 'HotelController@updateDetails');
        Route::post('create-hotel-features', 'HotelController@createFeature');
        Route::post('update-hotel-features', 'HotelController@updateFeature');
        Route::post('delete-hotel-features', 'HotelController@deleteFeature');
    });
});
