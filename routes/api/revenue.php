<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'admin', 'verified.user'], 'namespace' => 'Api\Revenue'], function () {
    Route::get('get-user-revenue-info', 'RevenueController@getRevenue');
});
