<?php


use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['auth:api', 'app.user', 'verified.user'], 'namespace' => 'Api\History'], function () {
    Route::get('get-user-expenditure-info', 'HistoryController@getExpenditure');
});
