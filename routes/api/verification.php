<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['language'], 'namespace' => 'Api\Auth'], function () {
    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('resend-email-verification-code', 'AuthController@resendEmailVerificationCode')->name('api.resendEmailVerificationCode');
        Route::post('email-verification', 'AuthController@emailVerify')->name('api.emailVerification');
        Route::group(['middleware' => 'verified.user'], function () {
            Route::get('send-phone-verification-code', 'AuthController@sendPhoneVerificationCode')->name('api.sendPhoneVerificationCode');
            Route::post('phone-verification', 'AuthController@phoneVerify')->name('api.phoneVerification');
        });
    });
});
