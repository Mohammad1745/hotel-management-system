<?php


use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'verified.user'], 'namespace' => 'Api\Room'], function () {
    Route::get('get-all-rooms', 'RoomController@roomIndex');
    Route::post('filter-rooms', 'RoomController@filterRoom');
    Route::post('room-details', 'RoomController@roomDetails');

    Route::group(['middleware' => 'admin'], function () {
        Route::post('create-room', 'RoomController@createRoom');
        Route::post('upload-room-image', 'RoomController@uploadImage');
        Route::post('update-room', 'RoomController@updateRoom');
        Route::post('delete-room', 'RoomController@deleteRoom');
    });
});
